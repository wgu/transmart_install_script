
## Clone the git and go to installation folder

```sh
git clone https://git-r3lab.uni.lu/wgu/transmart_install_script.git
cd transmart_install_script/sysadmin/
```

## Run installation script

* After script finished, ctrl+c may needed. Need to fix this

```sh
./tm_install.sh
```

## Kernel upgrading

* Manual reboot is needed after this step

```sh
./kernel_upgrade.sh
```

## Post installation

* tm_cz default password is tm_cz (will be asked in the script)

```sh
./post_tm_install.sh
```

## Source of the scripts (!NOT! part of the installation step, only if you are interested):

* based on the tranSMART foundation release

```
curl http://library.transmartfoundation.org/release/release16_1_0_artifacts/Scripts-release-16.1.zip -o Scripts-release-16.1.zip
unzip Scripts-release-16.1.zip
mv Scripts-release-16.1 Scripts
```

