# Migrate tranSMART DB

## On old VM

```sh
pg_dump -d transmart |gzip > tm_db_dump.sql.gz
# scp tm_db_dump.sql.gz to new VM
```

## On new VM

```sh
# stop tomcat7
sudo service tomcat7 stop

# recover from db_dump
cd FOLDER_OF_DB_DUMP
sudo su postgres
psql -c "DROP DATABASE transmart"
psql -d template1 -c "CREATE DATABASE transmart TEMPLATE template0 ENCODING='UTF-8' LC_COLLATE='en_US.UTF-8'"
cat tm_db_dump.sql.gz | psql -d transmart
exit

# restart services
sudo service postgresql restart
sudo service tomcat7 restart
```
