#!/bin/bash

# install dependencies
#sudo apt-get install -y curl unzip

# add the user transmart and add it to the sudo group
user_exist=false
getent passwd transmart $1 >/dev/null 2>&1 && user_exist=true

if $user_exist; then
    echo "user transmart already exist"
else
    sudo adduser transmart --disabled-password --gecos ""
    sudo adduser transmart sudo
    sudo echo "%transmart ALL=(ALL) NOPASSWD: ALL" > 200_transmart
    sudo mv 200_transmart /etc/sudoers.d/
    sudo chown root:root /etc/sudoers.d/200_transmart
fi

# add repository of java-7 
#sudo add-apt-repository -y ppa:openjdk-r/ppa
#sudo apt-get update

#
# run as transmart user for the following part
#

cd /home/transmart/
#sudo -u transmart -H bash -c "curl http://library.transmartfoundation.org/release/release16_1_0_artifacts/Scripts-release-16.1.zip -o Scripts-release-16.1.zip"
#sudo -u transmart -H bash -c "unzip Scripts-release-16.1.zip; mv Scripts-release-16.1 Scripts"

# clean old installation folders
sudo -u transmart -H bash -c "rm -fr Scripts transmart_install_script transmart"

# get the latest installation scripts
sudo -u transmart -H bash -c "git clone https://git-r3lab.uni.lu/wgu/transmart_install_script.git"
sudo -u transmart -H bash -c "mv transmart_install_script/Scripts ."

# run installation
sudo -u transmart -H bash -c "./Scripts/install-ubuntu/InstallTransmart.sh 2>&1 2>&1 | tee ~/install.log"
