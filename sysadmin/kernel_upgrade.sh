#!/bin/bash

sudo apt-get update && sudo apt-get upgrade
sudo apt-get -y install linux-generic linux-headers-generic linux-image-generic
sudo apt-get -y install --install-recommends linux-generic-lts-wily
sudo apt-get upgrade
echo "Please reboot the VM now..."
echo "sudo reboot"
