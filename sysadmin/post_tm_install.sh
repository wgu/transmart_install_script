#!/bin/bash

# install and config apache2
sudo apt-get install -y apache2

# use the config from git
sudo mv ~/transmart_install_script/Config/server.xml /etc/tomcat7/
sudo mv ~/transmart_install_script/Config/000-default.conf /etc/apache2/sites-available/
sudo mv ~/transmart_install_script/Config/postgresql.conf /etc/postgresql/9.3/main/
sudo mv ~/transmart_install_script/Config/Config.groovy /usr/share/tomcat7/.grails/transmartConfig/
sudo mv ~/transmart_install_script/Config/transmartlogo.png /var/lib/tomcat7/webapps/transmart/images/
sudo mv ~/transmart_install_script/Config/provider_logo.png /var/lib/tomcat7/webapps/transmart/images/
sudo mv ~/transmart_install_script/Config/index.html /var/www/html

# make all changes effective
sudo service postgresql restart
sudo service tomcat7 restart
sudo a2enmod proxy proxy_ajp
sudo service apache2 restart

# disable some users:
psql -U tm_cz -d transmart -h localhost -f ~/transmart_install_script/Config/disable_user.sql

# install R packages for SmartR
cp smart_dep.R /tmp
cd /tmp
sudo -u transmart -H bash -c "/tmp/smart_dep.R"

# setup TM services
cd /etc/init.d
sudo wget https://webdav-r3lab.uni.lu/public/biocore/transmart_releases/transmart-data-release-16.1/tm_services
sudo chmod +x tm_services
sudo kill `ps -efl |grep Rserve |grep quiet |awk '{print $4}'`
sudo /etc/init.d/tm_services start
